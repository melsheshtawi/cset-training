# Description

### [CSET-01] VPC Cloud Formation 
It’s required to write CloudFormation template that create the following resources:
- Resources 
    - VPC.
    - One public subnet in one Availability Zones.
    - Two private subnets in two different Availability Zones.
    - Internet Gateway.
    - Route Tables and Routes for the public subnet.
    - Two security groups named host and client.
        - Client should have inbound rule that allow access from your public ip
        - Host should have inbound rule that allow access only from Client Security group
- Parameters
    - VPC CIDR.
    - Subnets CIDRs.
    - Public IP. (your local machine IP)
- Outputs:
    - VPC ID
    - Subnets IDs
    - Security groups IDs

### Acceptance Criteria:
As a client, I want to run the CloudFormation template through AWS management console to create all the resources.